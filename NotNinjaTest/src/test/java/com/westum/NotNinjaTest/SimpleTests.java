package com.westum.NotNinjaTest;

import java.util.Random;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.*;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.westum.NotNinjaTest.pageobjectintersport.AccountPage;
import com.westum.NotNinjaTest.pageobjectintersport.HomePage;
import com.westum.NotNinjaTest.pageobjectintersport.LoginPage;
import com.westum.NotNinjaTest.pageobjectintersport.ProductDetailsPage;
import com.westum.NotNinjaTest.pageobjectintersport.ProductListPage;

enum Charsets {
	UPPER(65, 26),
	LOWER(97, 26),
	MIXED(33, 93),
	DIGITS(48, 10);
	
	private int lower;
	private int len;
	
	private Charsets(int lower, int len)
	{
		this.lower = lower;
		this.len = len;
	}
	
	public int lower()
	{
		return lower;
	}
	
	public int len()
	{
		return this.len;
	}
}

public class SimpleTests {
	WebDriver driver = new ChromeDriver();
	
	@BeforeMethod(alwaysRun = true)
	public void onTestStart() {
		
		driver.get("https://www.intersport.bg");
		driver.manage().window().maximize();
	}
	
	@AfterMethod(alwaysRun = true)
	public void onTestFinish() {
		driver.manage().deleteAllCookies();
		driver.quit();
	}
	
	@Test
	public void acne1() {
		
		// Verify logo is present on page
		WebElement logo = driver.findElement(By.className("header__logo-black"));
		Assert.assertTrue(logo.isDisplayed());
		
	}
	
	@Test
	public void acne2() {
		
		// Verify minicart is present on page
		Assert.assertTrue(driver.findElement(By.cssSelector("a[id='link__cart-other']")).isDisplayed());
		
		WebElement bag = driver.findElement(By.cssSelector("a[id='link__cart-other']"));
		// Verify minicart icon has text "Bag"
		Assert.assertEquals(bag.getText(), "Bag");
		Assert.assertEquals(bag.getAttribute("role"), "menuitem");
	}
	
	@Test
	public void acne3() throws InterruptedException {
		WebElement firstLevelCategory = driver.findElement(By.cssSelector("[class*='header__menu-item header__menu__category--fullscreen ']:nth-of-type(2)"));
		Actions action = new Actions(driver);
		action.moveToElement(firstLevelCategory).build().perform();
		driver.findElement(By.cssSelector("[aria-labelledby='link__shop-man']>[aria-labelledby='link__shop-man-2'] li[class='header__submenu-item ']:nth-of-type(5)")).click();
		String plpTitle = driver.findElement(By.cssSelector("[class*='grid-tile product-list__item-tile']:nth-of-type(4) a[class='name-link']")).getText();
		
		driver.findElement(By.cssSelector("[class*='grid-tile product-list__item-tile']:nth-of-type(4) a[class='name-link']")).click();
				
		// Verify product title on pdp
		Assert.assertTrue(driver.findElement(By.className("products-title")).getText().toLowerCase().contains(plpTitle.toLowerCase()));
	}
	
	@Test
	public void acne4() throws InterruptedException {
		WebElement firstLevelCategory = driver.findElement(By.cssSelector("[class*='header__menu-item header__menu__category--fullscreen ']:nth-of-type(2)"));
		Actions action = new Actions(driver);
		action.moveToElement(firstLevelCategory).build().perform();
		WebDriverWait wait = new WebDriverWait(driver, 60);
		driver.findElement(By.cssSelector("[aria-labelledby='link__shop-man']>[aria-labelledby='link__shop-man-2'] li[class='header__submenu-item ']:nth-of-type(5)")).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("[class*='grid-tile product-list__item-tile']:nth-of-type(4) a[class='name-link']")));
		driver.findElement(By.cssSelector("[class*='grid-tile product-list__item-tile']:nth-of-type(4) a[class='name-link']")).click();
		
		// We should pick size before adding to bag
		driver.findElement(By.cssSelector("[data-name^='dwvar']:nth-of-type(3)")).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("a[class$='add-to-cart']")));
		driver.findElement(By.cssSelector("a[class$='add-to-cart']")).click();
				
		// Verify the item count in header
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("[id='link__cart-other'] [class='basket-qty-container not-empty']")));
		Assert.assertEquals(driver.findElement(By.cssSelector("[id='link__cart-other'] [class='basket-qty-container not-empty']")).getText(), "1");
	}
	
	@Test
	public void intersport1() throws InterruptedException {
		HomePage home = new HomePage(driver);		
		ProductDetailsPage pdp = home.closeCookiePopup()
									 .selectSecondLevelCategory("Бельо")
									 .pickProduct()
									 .addToCart();
		
		Assert.assertEquals(pdp.getMinicartQuantity(), "1");
		
		
		/*WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("[id='CookiePopup'] button[class='btn']")));
		driver.findElement(By.cssSelector("[id='CookiePopup'] button[class='btn']")).click();
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("[id='CookiePopup'] button[class='btn']")));
		Actions action = new Actions(driver);
		action.moveToElement(driver.findElement(By.cssSelector("[id='main-navigation']>nav>ul>li>a"))).build().perform();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class = 'menufly open']//a[text() = 'Бельо']")));
		driver.findElement(By.xpath("//div[@class = 'menufly open']//a[text() = 'Бельо']")).click();
		driver.findElement(By.cssSelector("[class='thumb-link']")).click();
		Thread.sleep(3000);
		driver.findElement(By.id("add-to-cart")).click();
		wait.until(ExpectedConditions.textToBe(By.className("minicart-quantity"), "1"));
		
		// Verify minicart has opened.		
		Assert.assertEquals(driver.findElement(By.className("minicart-quantity")).getText(), "1");*/
	}
	
	@Test
	public void intersport2() throws InterruptedException {
		HomePage home = new HomePage(driver);
		LoginPage login = home.closeCookiePopup()
							  .openMyAccount();
		login.login("asd", "");
		Assert.assertEquals(login.getInvalidPasswordMessage(), "Това поле е задължително.");
		
		//Assert.assertEquals(account.getMyAccountText().toLowerCase(), "Моята сметка".toLowerCase());
		
		/*WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("[id='CookiePopup'] button[class='btn']")));
		driver.findElement(By.cssSelector("[id='CookiePopup'] button[class='btn']")).click();
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("[id='CookiePopup'] button[class='btn']")));
		driver.findElement(By.cssSelector("[class='header__submenu__myaccount ']>a")).click();*/
		//driver.findElement(By.cssSelector("[id*='dwfrm_login_username']")).sendKeys("fakeemail@westum.com");
		//driver.findElement(By.cssSelector("[id*='dwfrm_login_password']")).sendKeys("Intersport1!");
		//driver.findElement(By.cssSelector("[name='dwfrm_login_login']")).click();
		
		/*driver.findElement(By.cssSelector("[name='dwfrm_login_register']")).click();
		driver.findElement(By.id("dwfrm_profile_customer_firstname")).sendKeys("Nikol");
		driver.findElement(By.id("dwfrm_profile_customer_lastname")).sendKeys("Nikol");
		driver.findElement(By.id("dwfrm_profile_customer_phone")).sendKeys("0878345434");
		String randomEmail = randomString(10, Charsets.LOWER);
		String randomPassword = "Intersport1!"; // guaranteed to be random
		driver.findElement(By.id("dwfrm_profile_customer_email")).sendKeys(String.format("%s@westum.com", randomEmail));
		driver.findElement(By.id("dwfrm_profile_customer_emailconfirm")).sendKeys(String.format("%s@westum.com", randomEmail));
		driver.findElement(By.cssSelector("[id^='dwfrm_profile_login_passwordregister']")).sendKeys(randomPassword);
		driver.findElement(By.cssSelector("[id^='dwfrm_profile_login_passwordconfirm']")).sendKeys(randomPassword);
		driver.findElement(By.cssSelector("[for='dwfrm_profile_customer_termsagreementcustomerservice']")).click();
		driver.findElement(By.name("dwfrm_profile_confirm")).click();*/
		
		// Verify minicart has opened.
		//wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[text() = 'Актуализация']")));
		//Assert.assertEquals(driver.findElement(By.className("h4")).getText().toLowerCase(), "Моята сметка".toLowerCase());		
	}

	private static final Random rand = new Random();
	
	public static String randomString(int len, Charsets charset)
	{
		
		StringBuffer buf = new StringBuffer();
		for(int i = 0 ; i < len ; i++)
		{
			buf.append((char) (charset.lower() + rand.nextInt(charset.len())));
		}
		return buf.toString();
	}
}
