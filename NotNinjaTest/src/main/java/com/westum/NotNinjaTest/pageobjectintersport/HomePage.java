package com.westum.NotNinjaTest.pageobjectintersport;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomePage {
	private WebDriver wdw;
	private WebDriverWait wait;
	
	private final By LOC_COOKIE_POPUP_CLOSE = By.cssSelector("[id='CookiePopup'] button[class='btn']");
	private final By LOC_TOP_LEVEL_CATEGORY = By.cssSelector("[id='main-navigation']>nav>ul>li>a");
	private final By LOC_ACCOUNT = By.cssSelector("[class='header__submenu__myaccount ']>a");
	
	private final String locSecondLevelCategory = "//div[@class = 'menufly open']//a[text() = '%s']";
		
	public HomePage(WebDriver wd) {
		wdw = wd;
		wait = new WebDriverWait(wdw, 60);
	}
	
	public HomePage closeCookiePopup() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(LOC_COOKIE_POPUP_CLOSE));
		wdw.findElement(LOC_COOKIE_POPUP_CLOSE).click();
		wait.until(ExpectedConditions.invisibilityOfElementLocated(LOC_COOKIE_POPUP_CLOSE));
		
		return this;
	}
	
	public ProductListPage selectSecondLevelCategory(String secondLevelCategory) {
		Actions action = new Actions(wdw);
		action.moveToElement(wdw.findElement(LOC_TOP_LEVEL_CATEGORY)).build().perform();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(String.format(locSecondLevelCategory, secondLevelCategory))));
		wdw.findElement(By.xpath(String.format(locSecondLevelCategory, secondLevelCategory))).click();
		
		return new ProductListPage(wdw);
	}
	
	public LoginPage openMyAccount() {
		wdw.findElement(LOC_ACCOUNT).click();
		
		return new LoginPage(wdw);
	}
}
