package com.westum.NotNinjaTest.pageobjectintersport;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ProductListPage {
	private WebDriver wdw;
	private WebDriverWait wait;
	
	private final By LOC_PRODUCT = By.className("thumb-link");

	public ProductListPage(WebDriver wd) {
		wdw = wd;
		wait = new WebDriverWait(wdw, 60);
	}
	
	public ProductDetailsPage pickProduct() throws InterruptedException {
		wdw.findElement(LOC_PRODUCT).click();
		Thread.sleep(3000);
		
		return new ProductDetailsPage(wdw);
	}
}
