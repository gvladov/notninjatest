package com.westum.NotNinjaTest.pageobjectintersport;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ProductDetailsPage {
	private WebDriver wdw;
	private WebDriverWait wait;
	
	private final By LOC_ADD_TO_CART = By.id("add-to-cart");
	private final By LOC_MINICART_QUANTITY = By.className("minicart-quantity");

	public ProductDetailsPage(WebDriver wd) {
		wdw = wd;
		wait = new WebDriverWait(wdw, 60);
	}
	
	public ProductDetailsPage addToCart() {
		wdw.findElement(LOC_ADD_TO_CART).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(LOC_MINICART_QUANTITY));
		
		return this;
	}
	
	public String getMinicartQuantity() {
		return wdw.findElement(LOC_MINICART_QUANTITY).getText();
	}
}
