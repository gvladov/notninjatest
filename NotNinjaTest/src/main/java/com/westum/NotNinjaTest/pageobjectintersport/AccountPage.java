package com.westum.NotNinjaTest.pageobjectintersport;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AccountPage {
	private WebDriver wdw;
	private WebDriverWait wait;
	
	private final By LOC_ACCOUNT_HEADLINE = By.xpath("//a[text() = 'Актуализация']");
	private final By LOC_MY_ACCOUNT_TEXT = By.className("h4");
	
	public AccountPage(WebDriver wd) {
		wdw = wd;
		wait = new WebDriverWait(wdw, 60);
	}
	
	public String getMyAccountText() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(LOC_ACCOUNT_HEADLINE));
		return wdw.findElement(LOC_MY_ACCOUNT_TEXT).getText();
	}
}
