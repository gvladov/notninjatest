package com.westum.NotNinjaTest.pageobjectintersport;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage {
	private WebDriver wdw;
	private WebDriverWait wait;
	
	private final By LOC_USERNAME = By.cssSelector("[id*='dwfrm_login_username']");
	private final By LOC_PASSWORD = By.cssSelector("[id*='dwfrm_login_password']");
	private final By LOC_PASSWORD_INVALID = By.cssSelector("span[id*='dwfrm_login_password']");
	private final By LOC_LOGIN_BUTTON = By.cssSelector("[name='dwfrm_login_login']");
	
	public LoginPage(WebDriver wd) {
		wdw = wd;
		wait = new WebDriverWait(wdw, 60);
	}
	
	public LoginPage typeUsername(String username) {
		wdw.findElement(LOC_USERNAME).sendKeys(username);
		
		return this;
	}
	
	public LoginPage typePassword(String password) {
		wdw.findElement(LOC_PASSWORD).sendKeys(password);
		
		return this;
	}
	
	public LoginPage clickLogin() {
		wdw.findElement(LOC_LOGIN_BUTTON).click();
		
		return this;
	}
	
	public String getUsernameInput() {
		return wdw.findElement(LOC_USERNAME).getAttribute("value");
	}
	
	public AccountPage login(String username, String password) {
		typeUsername(username);
		typePassword(password);
		clickLogin();
		
		return new AccountPage(wdw);
	}
	
	public String getInvalidPasswordMessage() {
		return wdw.findElement(LOC_PASSWORD_INVALID).getText();
	}
}
